﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XLSXImport.Extensions.Collections
{
    static class CollectionExtensions
    {
        public static bool UnsortedEqual<T>(this ICollection<T> self, ICollection<T> other)
        {
            var dictionary = new Dictionary<T, int>();
            foreach (var k in self)
            {
                if (dictionary.ContainsKey(k)) dictionary[k]++;
                else dictionary[k] = 1;
            }
            foreach (var k in other)
            {
                if (!dictionary.ContainsKey(k)) return false;
                else dictionary[k]--;
            }
            return dictionary.Values.All(count => count == 0);
        }
    }
}
