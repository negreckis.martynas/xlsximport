﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XLSXImport
{
    static class Program
    {
        private static MainForm form;
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.ThreadException += new ThreadExceptionEventHandler(ThreadException);
            form = new MainForm();
            Application.Run(form);
        }

        static void ThreadException(object sender, ThreadExceptionEventArgs e)
        {
            using (var dialog = new ErrorDialog()) {
                dialog.SetException(e.Exception).ShowDialog(form);
            }
        }
    }
}
