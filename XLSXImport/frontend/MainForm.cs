﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using XLSXImport.Data;
using XLSXImport.DataType;
using XLSXImport.Tasks;

namespace XLSXImport
{
    public partial class MainForm : Form
    {
        private string InputFile { get; set; }
        private string OutputFile { get; set; }

        private dynamic dataType { get; set; }

        public MainForm()
        {
            InitializeComponent();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void inputFileButton_Click(object sender, EventArgs e)
        {
            InputFileDialog();
        }

        private void InputFileDialog()
        {
            openFileDialog.Filter = "Excel files (*.xlsx)|*xlsx|CSV files (*.csv)|*.csv|Other files|*";
            var result = openFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                InputFile = openFileDialog.FileName;
                inputFilePathLabel.Text = InputFile;
                InferDataType();
                EnableSubmitButton();
            }
        }

        private void OutputFileDialog()
        {
            saveFileDialog.Filter = "SQL files|*.sql;*.sqlite|All files|*";
            var result = saveFileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                OutputFile = saveFileDialog.FileName;
                outputFilePathLabel.Text = OutputFile;
                NewFileLabel.Visible = !File.Exists(OutputFile);
                EnableSubmitButton();
            }
        }

        private void EnableSubmitButton()
        {
            if (InputFile != null && OutputFile != null && dataType != null) SubmitButton.Enabled = true;
            else SubmitButton.Enabled = false;
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            (int recordsInserted, string tableName) = new ImportDataFromFile(InputFile, OutputFile, dataType).Run();
            using (var dialog = new SuccessDialog(OutputFile, tableName, recordsInserted))
            {
                dialog.ShowDialog(this);
                ResetForm();
            }
        }

        private void outputFileButton_Click(object sender, EventArgs e)
        {
            OutputFileDialog();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            dataType = new UsersDataType();
            EnableSubmitButton();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            dataType = new MaterialsDataType();
            EnableSubmitButton();
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            dataType = new ComponentsDataType();
            EnableSubmitButton();
        }

        private void InferDataType()
        {
            dataType = new InferDataType(InputFile).Run();
            Type type = (Type)dataType.TypeOfData;
            if (type == typeof(User)) radioButton1.Checked = true;
            if (type == typeof(Material)) radioButton2.Checked = true;
            if (type == typeof(Data.Component)) radioButton3.Checked = true;
            Console.WriteLine(type);
        }

        private void ResetForm()
        {
            InputFile = null;
            OutputFile = null;
            inputFilePathLabel.Text = "";
            outputFilePathLabel.Text = "";
            radioButton1.Checked = false;
            radioButton2.Checked = false;
            radioButton3.Checked = false;
            dataType = null;
        }
    }
}
