﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace XLSXImport
{
    public partial class SuccessDialog : Form
    {
        private readonly string filepath;
        private readonly string tableName;
        private readonly int recordsAdded;

        public SuccessDialog(string filepath, string tableName, int recordsAdded)
        {
            InitializeComponent();
            this.filepath = filepath;
            this.tableName = tableName;
            this.recordsAdded = recordsAdded;
            FillDescription();
        }

        private void FillDescription()
        {
            FilepathLabel.Text = $"Duombazė '{filepath}'";
            InfoLabel.Text = $"Įkelta {recordsAdded} įrašų į '{tableName}' lentelę";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
