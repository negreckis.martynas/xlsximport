﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XLSXImport.Data
{
    public class User
    {
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; } //TODO: encryption
        public string company { get; set; }
        public string email{ get; set; }
        public int role{ get; set; }
        public string client_code{ get; set; }

        public override bool Equals(object obj)
        {
            return obj is User user &&
                   id == user.id &&
                   username == user.username &&
                   password == user.password &&
                   company == user.company &&
                   email == user.email &&
                   role == user.role &&
                   client_code == user.client_code;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(id, username, password, company, email, role, client_code);
        }
    }

    public class Material
    {
        public int id{ get; set; }
        public string group{ get; set; }
        public string name{ get; set; }
        public string description{ get; set; }
        public string unit{ get; set; }
        public decimal weight{ get; set; }
        public decimal coef{ get; set; }
        public int waste{ get; set; }
        public decimal price{ get; set; }
        public string ax{ get; set; }
        public string ax_name{ get; set; }
        public string comment{ get; set; }

        public override bool Equals(object obj)
        {
            return obj is Material material &&
                   id == material.id &&
                   group == material.group &&
                   name == material.name &&
                   description == material.description &&
                   unit == material.unit &&
                   weight == material.weight &&
                   coef == material.coef &&
                   waste == material.waste &&
                   price == material.price &&
                   ax == material.ax &&
                   ax_name == material.ax_name &&
                   comment == material.comment;
        }

        public override int GetHashCode()
        {
            HashCode hash = new HashCode();
            hash.Add(id);
            hash.Add(group);
            hash.Add(name);
            hash.Add(description);
            hash.Add(unit);
            hash.Add(weight);
            hash.Add(coef);
            hash.Add(waste);
            hash.Add(price);
            hash.Add(ax);
            hash.Add(ax_name);
            hash.Add(comment);
            return hash.ToHashCode();
        }
    }

    public class Component
    {
        public int id{ get; set; }
        public string in1{ get; set; }
        public string in2{ get; set; }
        public string in3{ get; set; }
        public string in4{ get; set; }
        public string in5{ get; set; }
        public int quantity{ get; set; }
        public string description{ get; set; }
        public string material1{ get; set; }
        public string material2{ get; set; }
        public string material3{ get; set; }
        public string material4{ get; set; }

        public override bool Equals(object obj)
        {
            return obj is Component component &&
                   id == component.id &&
                   in1 == component.in1 &&
                   in2 == component.in2 &&
                   in3 == component.in3 &&
                   in4 == component.in4 &&
                   in5 == component.in5 &&
                   quantity == component.quantity &&
                   description == component.description &&
                   material1 == component.material1 &&
                   material2 == component.material2 &&
                   material3 == component.material3 &&
                   material4 == component.material4;
        }

        public override int GetHashCode()
        {
            HashCode hash = new HashCode();
            hash.Add(id);
            hash.Add(in1);
            hash.Add(in2);
            hash.Add(in3);
            hash.Add(in4);
            hash.Add(in5);
            hash.Add(quantity);
            hash.Add(description);
            hash.Add(material1);
            hash.Add(material2);
            hash.Add(material3);
            hash.Add(material4);
            return hash.ToHashCode();
        }
    }
}
