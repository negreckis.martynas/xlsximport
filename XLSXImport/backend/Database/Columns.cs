﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XLSXImport.backend.Database
{
    public static class Column
    {
        public static IColumn OfType(Type type, string name)
        {
            if (type == typeof(int)) return Int(name);
            if (type == typeof(string)) return String(name);
            if (type == typeof(decimal)) return Decimal(name);
            throw new InvalidCastException($"Unexpected type: {type.Name}");
        }

        public static IntColumn PrimaryKey(string name) => new IntColumn(name, true);
        public static IntColumn Int(string name) => new IntColumn(name, false);
        public static StringColumn String(string name) => new StringColumn(name);
        public static DecimalColumn Decimal(string name) => new DecimalColumn(name);
    }

    public interface IColumn
    {
        string Name { get; } 
        string CreateTableString();
        object GetValue(SQLiteDataReader reader, int ordinal);

        DbType DbType { get; }
    }

    public class IntColumn : IColumn
    {
        public string Name { get; private set; }
        private readonly bool primaryKey;

        public IntColumn(string name, bool primaryKey = false)
        {
            this.Name = name;
            this.primaryKey = primaryKey;
        }

        public string CreateTableString() => $"`{Name}` INTEGER{(primaryKey?" PRIMARY KEY":"")}";

        public object GetValue(SQLiteDataReader reader, int ordinal) => reader.GetInt32(ordinal);

        public DbType DbType => DbType.Int32;
    }

    public class StringColumn : IColumn
    {
        public string Name { get; private set; }

        public DbType DbType => DbType.String;

        public StringColumn(string name) => this.Name = name;

        public string CreateTableString() => $"`{Name}` TEXT";

        public object GetValue(SQLiteDataReader reader, int ordinal) => reader.GetString(ordinal);
    }

    public class DecimalColumn : IColumn
    {
        public string Name { get; private set; }

        public DbType DbType => DbType.Decimal;

        public DecimalColumn(string name) => this.Name = name;

        public string CreateTableString() => $"`{Name}` DECIMAL(10,2)";

        public object GetValue(SQLiteDataReader reader, int ordinal) => decimal.Parse(reader.GetValue(ordinal).ToString());
    }
}
