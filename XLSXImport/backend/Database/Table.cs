﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XLSXImport.DataType;

namespace XLSXImport.backend.Database
{
    public class Table<T>
    {
        //TODO: table validation
        protected string TableName { get; set; }
        private IColumn[] columns;

        public Table(IDataType<T> dataType)
        {
            TableName = dataType.TableName;
            columns = dataType.TableColumns();
        }

        public static Table<T> ForDataType(IDataType<T> datatype) => new Table<T>(datatype);//trick to infer generic from argument

        private string GenerateTableQuery(bool temporary = false)
        {
            return $"CREATE {(temporary?"TEMPORARY ":"")}TABLE IF NOT EXISTS `{TableName}`({string.Join(",", columns.Select(c => c.CreateTableString()))});";
        }

        private void EnsureTable(SQLiteConnection connection)
        {
            if (!TableExists(connection)) CreateTable(connection);
        }

        private bool TableExists(SQLiteConnection connection)
        {
            using(var reader = new SQLiteCommand($"SELECT `name` FROM `sqlite_master` WHERE `type`='table' AND `name`='{TableName}'", connection).ExecuteReader())
            {
                return reader.HasRows;
            }
        }

        public void CreateTable(SQLiteConnection connection)
        {
            new SQLiteCommand(GenerateTableQuery(), connection).ExecuteNonQuery();
        }

        private SQLiteCommand CreateInsertPreparedStatement(SQLiteConnection connection)
        {
            var properties = typeof(T).GetProperties().Select(prop => prop.Name);
            var command =  new SQLiteCommand($"INSERT INTO {TableName} ({string.Join(",", properties.Select(n => $"`{n}`"))}) VALUES ({string.Join(",", properties.Select(_ => "?"))});", connection);
            command.Prepare();
            return command;
        }

        public int Insert(IEnumerable<T> data, SQLiteConnection connection)
        {
            EnsureTable(connection);
            int sum = 0;
            using (var transaction = connection.BeginTransaction())
            using (var insert = CreateInsertPreparedStatement(connection))
            {
                var parameters = new List<(string, SQLiteParameter)>();
                var type = typeof(T);
                foreach (IColumn column in columns)
                {
                    var p = new SQLiteParameter();
                    parameters.Add((column.Name, p));
                    insert.Parameters.Add(p); ;
                }

                foreach (T row in data)
                {
                    foreach(var (col, p) in parameters)
                    {
                        p.Value = type.GetProperty(col).GetValue(row);
                    }
                    sum += insert.ExecuteNonQuery();
                }
                
                transaction.Commit();
                //TODO: create parameters once, send query, edit parameter value for other query
            }
            return sum;
        }


    }
}
