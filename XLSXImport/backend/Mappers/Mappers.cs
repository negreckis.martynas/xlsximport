﻿using System;
using System.Collections.Generic;
using XLSXImport.Data;

namespace XLSXImport.Mappers
{
    public interface IMapper<in I, out O>
    {
        IEnumerable<O> Map(IEnumerable<I> input);
    }

    public class ComponentsTrimmingMapper : IMapper<Component, Component>
    {
        public IEnumerable<Component> Map(IEnumerable<Component> input)
        {
            foreach(var comp in input)
            {
                yield return new Component()
                {
                    id = comp.id,
                    in1 = comp.in1.Trim(),
                    in2 = comp.in2.Trim(),
                    in3 = comp.in3.Trim(),
                    in4 = comp.in4.Trim(),
                    in5 = comp.in5.Trim(),
                    description = comp.description.Trim(),
                    quantity = comp.quantity,
                    material1 = comp.material1.Trim(),
                    material2 = comp.material2.Trim(),
                    material3 = comp.material3.Trim(),
                    material4 = comp.material4.Trim(),
                };
            }
        }
    }

    public class IdentityMapper<T> : IMapper<T, T>
    {
        public IEnumerable<T> Map(IEnumerable<T> input)
        {
            foreach(var item in input)
            {
                yield return item;
            }
        }
    }
}