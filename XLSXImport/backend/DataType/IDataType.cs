﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XLSXImport.backend.Database;
using XLSXImport.Mappers;

namespace XLSXImport.DataType
{
    public interface IDataType<T>
    {
        string[] Headers { get; }
        string TableName { get; }

        IColumn[] TableColumns();

        //TODO: better name
        IMapper<T, T> GetCorrectionMapper();

        Type TypeOfData { get; }
    }
}
