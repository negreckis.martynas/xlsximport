﻿using System;
using System.Linq;
using XLSXImport.backend.Database;
using XLSXImport.Data;
using XLSXImport.Mappers;

namespace XLSXImport.DataType
{
    public class UsersDataType : IDataType<User>
    {
        public string[] Headers => new string[] { "id", "username", "password", "company", "email", "role", "client_code" };

        public IColumn[] TableColumns()
        {
            var propsAsColumns = typeof(User).GetProperties()
                                             .Where(p => p.Name != "id")
                                             .Select(p => Column.OfType(p.PropertyType, p.Name));
            return new IColumn[] { Column.PrimaryKey("id") }.Concat(propsAsColumns).ToArray();
        }

        public IMapper<User, User> GetCorrectionMapper() => new IdentityMapper<User>();

        public string TableName => "users";

        public Type TypeOfData => typeof(User);
    }
}
