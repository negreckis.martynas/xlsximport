﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XLSXImport.backend.Database;
using XLSXImport.Data;
using XLSXImport.Mappers;

namespace XLSXImport.DataType
{
    public class MaterialsDataType : IDataType<Material>
    {
        public string[] Headers => new string[] { "id", "group", "name", "description", "unit", "weight", "coef", "waste", "price", "ax", "ax_name", "comment" };

        public string TableName => "materials";

        public Type TypeOfData => typeof(Material);

        public IMapper<Material, Material> GetCorrectionMapper() => new IdentityMapper<Material>();

        public IColumn[] TableColumns()
        {
            var propsAsColumns = typeof(Material).GetProperties()
                                                 .Where(p => p.Name != "id")
                                                 .Select(p => Column.OfType(p.PropertyType, p.Name));
            return new IColumn[] { Column.PrimaryKey("id") }.Concat(propsAsColumns).ToArray();
        }
    }
}
