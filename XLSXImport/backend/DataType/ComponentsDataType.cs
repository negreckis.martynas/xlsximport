﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XLSXImport.backend.Database;
using XLSXImport.Data;
using XLSXImport.Mappers;

namespace XLSXImport.DataType
{
    public class ComponentsDataType : IDataType<Component>
    {
        public string[] Headers => new string[] { "id", "in1", "in2", "in3", "in4", "in5", "quantity", "description", "material1", "material2", "material3", "material4" };

        public string TableName => "components";

        public Type TypeOfData => typeof(Component);

        public IMapper<Component, Component> GetCorrectionMapper() => new ComponentsTrimmingMapper();

        public IColumn[] TableColumns()
        {
            var propsAsColumns = typeof(Component).GetProperties()
                                                  .Where(p => p.Name != "id")
                                                  .Select(p => Column.OfType(p.PropertyType, p.Name));
            return new IColumn[] { Column.PrimaryKey("id") }.Concat(propsAsColumns).ToArray();
        }

    }
}
