﻿using System;
using XLSXImport.DataType;

namespace XLSXImport.Tasks
{
    public class InferDataType
    {
        private DataSource.DataSource dataSource;
        private string file;

        public InferDataType(string file)
        {
            this.file = file;
        }

        public InferDataType(DataSource.DataSource dataSource)
        {
            this.dataSource = dataSource;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>IDataType<dynamic></returns>
        public dynamic Run()
        {
            if (dataSource == null)
            {
                using (var source = DataSource.DataSource.InferFromFile(file))
                {
                    return Infer(source);
                }
            }
            else return Infer(dataSource);
        }

        private dynamic Infer(DataSource.DataSource dataSource)
        {
            if (dataSource.Verify(new UsersDataType())) return new UsersDataType();
            if (dataSource.Verify(new MaterialsDataType())) return new MaterialsDataType();
            if (dataSource.Verify(new ComponentsDataType())) return new ComponentsDataType();
            throw new Exception("Data does not match any known type");
        }
    }
}
