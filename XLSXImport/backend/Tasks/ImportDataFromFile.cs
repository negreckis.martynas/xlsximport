﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using XLSXImport.backend.Database;
using XLSXImport.DataSource;
using XLSXImport.DataType;

namespace XLSXImport.Tasks
{
    public class ImportDataFromFile
    {
        private readonly string inputFile;
        private readonly string outputFile;
        private dynamic dataType; //IDataType<dynamic>

        public ImportDataFromFile(string inputFile, string outputFile, dynamic dataType = null)
        {
            this.inputFile = inputFile;
            this.outputFile = outputFile;
            this.dataType = dataType;
        }

        public (int, string) Run()
        {
            using (SQLiteConnection connection = CreateConnection())
            using (DataSource.DataSource dataSource = DataSource.DataSource.InferFromFile(inputFile))
            {
                if (dataType == null) dataType = new InferDataType(dataSource).Run();

                connection.Open();
                Type tableType = typeof(Table<>).MakeGenericType(dataType.TypeOfData); //Type of Table<type>
                dynamic table = Activator.CreateInstance(tableType, new object[] { dataType }); //Table<type>
                dynamic data = dataType.GetCorrectionMapper().Map(dataSource.ParseRows(dataType)); //IEnumerable<type>
                int recordsAdded = (int) table.Insert(data, connection);
                string tableName = (string)dataType.TableName;
                return (recordsAdded, tableName);
            }
        }

        private SQLiteConnection CreateConnection()
        {
            if (!File.Exists(outputFile)) SQLiteConnection.CreateFile(outputFile);
            return new SQLiteConnection($"Data Source={outputFile};Version=3");
        }
    }
}
