﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using XLSXImport.DataType;
using XLSXImport.Extensions.Collections;

namespace XLSXImport.DataSource
{
    //TODO: think of a way to handle multiple-sheet documents
    public class XLSXDataSource : DataSource
    {
        private FileStream stream;
        private IExcelDataReader reader;
        private string[] headers;

        public XLSXDataSource(string filename)
        {
            stream = File.OpenRead(filename);
            reader = ExcelReaderFactory.CreateReader(stream);
            ReadHeaders();
        }

        private void ReadHeaders()
        {
            reader.Read();
            headers = new string[reader.FieldCount];
            for (int i = 0; i < reader.FieldCount; i++)
            {
                headers[i] = reader.GetString(i);
            }
        }

        public override void Dispose()
        {
            if (stream == null) return;
            stream.Dispose();
            reader.Dispose();
        }

        public override IEnumerable<T> ParseRows<T>()
        {
            while(reader.Read())
            {
                T obj = new T();
                object obj2 = obj;
                Type type = obj.GetType();
                for(int i = 0; i < headers.Length; i++)
                {
                    var prop = type.GetProperty(headers[i]);
                    var val = reader.GetValue(i);
                    var val2 = InternalTypeConversion.Convert(val, prop.PropertyType);
                    prop.SetValue(obj2, val2);
                }
                yield return (T)obj2;
            }
        }

        public override bool Verify<T>(IDataType<T> dataType)
        {
            return headers.UnsortedEqual(dataType.Headers);
        }
    }


    /// <summary>
    /// Maps received types from ExcelDataReader to DTO property types.
    /// Should convert from every type that can be returned from ExcelDataReader.GetValue(int); at the time of writing types were:
    ///     double, int, bool, DateTime, TimeSpan, string, or null.
    /// Add more result types depending when needed, throw exception if type is unexpected.
    /// </summary>
    /// <see cref="https://github.com/ExcelDataReader/ExcelDataReader"/>
    static class InternalTypeConversion
    {
        private static Dictionary<Type, Func<object, Type, object>> typeMapping = new Dictionary<Type, Func<object, Type, object>>()
        {
            { typeof(int), ConvertInt },
            { typeof(double), ConvertDouble },
            { typeof(string), ConvertString }
            //null is special case, so it is
        };

        internal static object Convert(object val, Type fieldType)
        {
            if (val == null) return ConvertNull(fieldType);
            if (val.GetType() == fieldType) return val; //no conversion needed if type matches, checks to same type will never match in other methods
            return typeMapping[val.GetType()](val, fieldType);
        }

        private static object ConvertInt(object val, Type fieldType)
        {
            int num = (int)val;
            if (fieldType == typeof(double)) return (double)num;
            if (fieldType == typeof(decimal)) return (decimal)num;
            if (fieldType == typeof(string)) return num.ToString();
            throw new InvalidCastException($"Unmapped conversion: int to {fieldType.Name}");
        }

        private static object ConvertDouble(object val, Type fieldType)
        {
            double num = (double)val;
            if (fieldType == typeof(decimal)) return (decimal)num;
            if (fieldType == typeof(int))
            {
                if ((double)val % 1 == 0) return (int)num;
                else throw new InvalidCastException($"Precision lost when casting double to int, value is {val}");
            }
            if (fieldType == typeof(string)) return num.ToString();
            throw new InvalidCastException($"Unmapped conversion: double to {fieldType.Name}");
        }

        private static object ConvertString(object val, Type fieldType)
        {
            string str = (string)val;
            if (str.Length == 0) return ConvertNull(fieldType); // if cell has empty string but expecting not string, use null results. Need separate method if expected values diverge later on
            if (fieldType == typeof(int)) return int.Parse(str);
            if (fieldType == typeof(double)) return double.Parse(str);
            if (fieldType == typeof(decimal)) return decimal.Parse(str);
            throw new InvalidCastException($"Unmapped conversion: string to {fieldType.Name}");
        }

        private static object ConvertNull(Type fieldType)
        {
            if (fieldType == typeof(int)) return 0;
            if (fieldType == typeof(bool)) return false;
            if (fieldType == typeof(double)) return 0.0;
            if (fieldType == typeof(decimal)) return 0m;
            if (fieldType == typeof(string)) return null;
            throw new InvalidCastException($"Unmapped conversion: null to {fieldType.Name}");
        }
    }
}
