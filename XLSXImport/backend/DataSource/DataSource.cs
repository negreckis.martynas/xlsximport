﻿using System;
using System.Collections.Generic;
using System.IO;
using XLSXImport.Data;
using XLSXImport.DataType;

namespace XLSXImport.DataSource
{
    public abstract class DataSource : IDisposable
    {
        public abstract bool Verify<T>(IDataType<T> dataType);

        public abstract IEnumerable<T> ParseRows<T>() where T : new();

        public IEnumerable<T> ParseRows<T>(IDataType<T> dataType) where T : new() => ParseRows<T>();

        public abstract void Dispose();

        public static DataSource InferFromFile(string filePath)
        {
            //TODO check file signatures?
            var extension = Path.GetExtension(filePath);
            if (extension == ".xlsx" || extension == ".xlsx") return new XLSXDataSource(filePath);
            //assume .csv and other extensions are csv files
            return new CSVDataSource(filePath);
        }
    }
}
