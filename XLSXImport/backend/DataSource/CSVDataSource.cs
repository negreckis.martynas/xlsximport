﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using XLSXImport.DataType;
using XLSXImport.Extensions.Collections;

namespace XLSXImport.DataSource
{
    public class CSVDataSource : DataSource
    {
        private readonly string filename;
        private StreamReader stream;
        private CsvReader reader;

        private string[] headers;
        private bool hasNext = false;

        public CSVDataSource(string filename)
        {
            this.filename = filename;
            OpenFile();
        }

        private void OpenFile()
        {
            if (reader != null) return;

            stream = new StreamReader(filename);
            reader = new CsvReader(stream, new CsvConfiguration(System.Globalization.CultureInfo.InvariantCulture) { 
                PrepareHeaderForMatch = TrimAndLowercaseHeaders
            });
            hasNext = reader.Read();
            reader.ReadHeader();
            headers = reader.HeaderRecord;
        }

        private string TrimAndLowercaseHeaders(PrepareHeaderForMatchArgs args) => args.Header.Trim().ToLower();

        public override bool Verify<T>(IDataType<T> dataType)
        {
            return headers.UnsortedEqual(dataType.Headers);
        }

        public override void Dispose()
        {
            if (stream != null) stream.Dispose();
            if (reader != null) reader.Dispose();
        }

        public override IEnumerable<T> ParseRows<T>()
        {
            while (reader.Read())
            {
                yield return reader.GetRecord<T>();
            }
        }
    }
}
