﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using XLSXImport.backend.Database;
using XLSXImport.Data;
using XLSXImport.DataSource;
using XLSXImport.DataType;
using XLSXImport.Mappers;

namespace XLSXImportTests.backend
{
    [TestClass()]
    [DeploymentItem(@"x86\SQLite.Interop.dll", "x86")]
    public class DatabaseTests
    {
        [TestMethod()]
        [DeploymentItem(@"files\Materials.xlsx")]
        public void MaterialsInsert()
        {
            using (var materials = new XLSXDataSource("Materials.xlsx"))
            using (var connection = new SQLiteConnection("Data Source=:memory:"))
            {
                connection.Open();
                var dataType = new MaterialsDataType();
                var selectQuery = "SELECT * FROM `materials` ORDER BY `name` DESC LIMIT 3";
                InsertTest(materials, dataType, connection, selectQuery).Invoke(result =>
                {
                    Assert.AreEqual("zz_88", result[0].name);
                    Assert.AreEqual(2, result[1].waste);
                    Assert.AreEqual(46.03m, result[2].price);
                });
                connection.Close();
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Users.xlsx")]
        public void UsersInsert()
        {
            using (var users = new XLSXDataSource("Users.xlsx"))
            using (var connection = new SQLiteConnection("Data Source=:memory:"))
            {
                connection.Open();
                var dataType = new UsersDataType();
                var selectQuery = "SELECT * FROM `Users` ORDER BY `company` ASC LIMIT 3";
                InsertTest(users, dataType, connection, selectQuery).Invoke(result =>
                {
                    Assert.AreEqual("fkaas7x", result[0].username);
                    Assert.AreEqual("ghuddlestone8c@tmall.com", result[1].email);
                    Assert.AreEqual(4, result[2].role);
                });
                connection.Close();
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Components.xlsx")]
        public void ComponentsInsert()
        {
            using (var components = new XLSXDataSource("Components.xlsx"))
            using (var connection = new SQLiteConnection("Data Source=:memory:"))
            {
                connection.Open();
                var dataType = new ComponentsDataType();
                var selectQuery = "SELECT * FROM `Components` ORDER BY `id` DESC LIMIT 3";
                InsertTest(components, dataType, connection, selectQuery).Invoke(result =>
                {
                    Assert.AreEqual("4", result[0].in1);
                    Assert.AreEqual("7.1", result[1].in5);
                    Assert.AreEqual("cp_type9|7", result[2].material3);
                });
                connection.Close();
            }
        }

        private Action<Action<List<T>>> InsertTest<T>(DataSource dataSource, IDataType<T> dataType, SQLiteConnection connection, string selectQuery) where T: new()
        {

            var data = dataType.GetCorrectionMapper().Map(dataSource.ParseRows(dataType));
            var table = Table<T>.ForDataType(dataType);

            table.Insert(data, connection);

            List<T> result;
            using (var reader = new SQLiteCommand(selectQuery, connection).ExecuteReader())
            {
                result = ReadResults(dataType, reader).ToList();
            }
            return (listConsumer) => listConsumer(result);
        }

        private IEnumerable<T> ReadResults<T>(IDataType<T> dataType, SQLiteDataReader reader) where T : new()
        {
            var columns = dataType.TableColumns();
            Type type = typeof(T);
            while (reader.Read())
            {
                T item = new T();         
                for (int i = 0; i < reader.FieldCount; i++)
                {
                    
                    type.GetProperty(columns[i].Name).SetValue(item, columns[i].GetValue(reader, i));
                }
                yield return item;
            }
        }
    }
}
