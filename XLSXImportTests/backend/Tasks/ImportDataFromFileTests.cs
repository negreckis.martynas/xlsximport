﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using XLSXImport.Tasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SQLite;

namespace XLSXImport.Tasks.Tests
{
    [TestClass()]
    [DeploymentItem(@"x86\SQLite.Interop.dll", "x86")]
    public class ImportDataFromFileTests
    {
        [TestMethod()]
        [DeploymentItem(@"files\Materials.xlsx")]
        public void RunTest()
        {
            new ImportDataFromFile("Materials.xlsx", "test.sqlite").Run();
            using (var connection = new SQLiteConnection("Data source=test.sqlite;Version=3"))
            {
                connection.Open();
                using (var reader =  new SQLiteCommand("SELECT `id` FROM `materials` ORDER BY `id` DESC LIMIT 1", connection).ExecuteReader()){
                    reader.Read();
                    Assert.AreEqual(1000, reader.GetInt32(0));
                }
                connection.Close();
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Materials.xlsx")]
        public void SpaceInNameTest()
        {
            new ImportDataFromFile("Materials.xlsx", "database with spaces.sqlite").Run();
            Assert.IsTrue(File.Exists("database with spaces.sqlite"));
            using (var connection = new SQLiteConnection("Data source='database with spaces.sqlite';Version=3"))
            {
                connection.Open();
                using (var reader = new SQLiteCommand("SELECT `id` FROM `materials` ORDER BY `id` DESC LIMIT 1", connection).ExecuteReader())
                {
                    reader.Read();
                    Assert.AreEqual(1000, reader.GetInt32(0));
                }
                connection.Close();
            }
        }
    }
}