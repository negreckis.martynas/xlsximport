﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using XLSXImport.Data;
using XLSXImport.DataType;

namespace XLSXImport.DataSource.Tests
{
    [TestClass()]
    public class XSLXDataSourceTests
    {
        [TestMethod()]
        [DeploymentItem(@"files\basic.xlsx")]
        public void BasicTest()
        {
            using (DataSource instance = new XLSXDataSource("basic.xlsx"))
            {
                DataSourceShared.BasicTest(instance);
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Users.xlsx")]
        public void UsersTest()
        {
            using (DataSource instance = new XLSXDataSource("Users.xlsx"))
            {
                DataSourceShared.UsersTest(instance);
            }
        }


        [TestMethod()]
        [DeploymentItem(@"files\Materials.xlsx")]
        public void MaterialsTest()
        {
            using (DataSource instance = new XLSXDataSource("Materials.xlsx"))
            {
                DataSourceShared.MaterialsTest(instance);
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Components.xlsx")]
        public void ComponentsTest()
        {
            using (DataSource instance = new XLSXDataSource("Components.xlsx"))
            {
                DataSourceShared.ComponentsTest(instance);
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Users.xlsx")]
        [DeploymentItem(@"files\Materials.xlsx")]
        [DeploymentItem(@"files\Components.xlsx")]
        public void ValidationTest()
        {
            using (DataSource users = new XLSXDataSource("Users.xlsx"))
            using (DataSource materials = new XLSXDataSource("Materials.xlsx"))
            using (DataSource components = new XLSXDataSource("Components.xlsx"))
            {
                var userDT = new UsersDataType();
                var materialDT = new MaterialsDataType();
                var componentDT = new ComponentsDataType();
                Assert.IsTrue(users.Verify(userDT));
                Assert.IsTrue(materials.Verify(materialDT));
                Assert.IsTrue(components.Verify(componentDT));

                Assert.IsFalse(users.Verify(materialDT));
                Assert.IsFalse(materials.Verify(componentDT));
                Assert.IsFalse(components.Verify(userDT));
                Assert.IsFalse(users.Verify(componentDT));
                Assert.IsFalse(materials.Verify(userDT));
                Assert.IsFalse(components.Verify(materialDT));
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Users.xlsx")]
        public void AccessThroughDataType()
        {
            using (DataSource users = new XLSXDataSource("Users.xlsx"))
            {
                var dataType = new UsersDataType();
                var result = users.ParseRows(dataType).Skip(50).Take(1).First();
                Assert.AreEqual(51, result.id);
                Assert.AreEqual("CodeDa9", result.client_code);
            }
        }
    }
}

