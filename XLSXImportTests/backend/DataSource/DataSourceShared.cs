﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XLSXImport.Data;

namespace XLSXImport.DataSource.Tests
{
    static class DataSourceShared
    {
        public static void BasicTest(DataSource source)
        {
            List<BasicData> expected = new List<BasicData>() { new BasicData { Id = 1, Name = "foo"},
                                                               new BasicData { Id = 2, Name = "bar"},
                                                               new BasicData { Id = 3, Name = "baz"}};
           
            var actual = source.ParseRows<BasicData>().ToList();
            CollectionAssert.AreEqual(expected, actual);
        }

        public static void UsersTest(DataSource source)
        {
            var users = source.ParseRows<User>().ToList();

            var u1 = users[0];
            Assert.AreEqual("sbosence0", u1.username);
            Assert.AreEqual("CodeXYs", u1.client_code);
            Assert.AreEqual(4, u1.role);

            var u2 = users[users.Count - 1];
            Assert.AreEqual("RRzfC7", u2.password);
            Assert.AreEqual("kbealerr@facebook.com", u2.email);
        }


        public static void MaterialsTest(DataSource source)
        {
            var materials = source.ParseRows<Material>().ToList();

            var m1 = materials[0];
            Assert.AreEqual("ri_48", m1.name);
            Assert.AreEqual(0.46m, m1.coef);
            Assert.AreEqual(9, m1.waste);
            Assert.AreEqual(2.9m, m1.weight);
            Assert.AreEqual("dictumst morbi vestibulum", m1.description);

            var m2 = materials[materials.Count - 1];
            Assert.AreEqual("NTF961373", m2.ax_name);
            Assert.AreEqual("auctor", m2.comment);
            Assert.AreEqual(5m, m2.weight);
            
        }

        public static void ComponentsTest(DataSource source)
        {
            var components = source.ParseRows<Component>().ToList();

            var c1 = components[0];
            Assert.AreEqual("4", c1.in1);
            Assert.AreEqual("gt_type4", c1.in2);
            Assert.AreEqual("odio", c1.in3);
            Assert.AreEqual(" mz6|7 ", c1.material1);

            var c2 = components[components.Count - 1];
            Assert.AreEqual(7, c2.quantity);
            Assert.AreEqual("kn_type9|6", c2.material4);
            Assert.AreEqual("ante ipsum primis", c2.description);
        }
    }
    class BasicData
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public override bool Equals(object obj)
        {
            return obj is BasicData data &&
                   Id == data.Id &&
                   Name == data.Name;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Name);
        }
    }
}
