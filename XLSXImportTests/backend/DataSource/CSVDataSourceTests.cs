﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using XLSXImport.DataType;

namespace XLSXImport.DataSource.Tests
{
    [TestClass()]
    public class CSVDataSourceTests
    {
        [TestMethod()]
        [DeploymentItem(@"files\basic.csv")]
        public void BasicTest()
        {
            using (DataSource instance = new CSVDataSource("basic.csv"))
            {
                DataSourceShared.BasicTest(instance);
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Users.csv")]
        public void UsersTest()
        {
            using (DataSource instance = new CSVDataSource("Users.csv"))
            {
                DataSourceShared.UsersTest(instance);
            }
        }
    

        [TestMethod()]
        [DeploymentItem(@"files\Materials.csv")]
        public void MaterialsTest()
        {
            using (DataSource instance = new CSVDataSource("Materials.csv"))
            {
                DataSourceShared.MaterialsTest(instance);
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Components.csv")]
        public void ComponentsTest()
        {
            using (DataSource instance = new CSVDataSource("Components.csv"))
            {
                DataSourceShared.ComponentsTest(instance);
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Users.csv")]
        [DeploymentItem(@"files\Materials.csv")]
        [DeploymentItem(@"files\Components.csv")]
        public void ValidationTest()
        {
            using (DataSource users = new CSVDataSource("Users.csv"))
            using (DataSource materials = new CSVDataSource("Materials.csv"))
            using (DataSource components = new CSVDataSource("Components.csv"))
            {
                var userDT = new UsersDataType();
                var materialDT = new MaterialsDataType();
                var componentDT = new ComponentsDataType();
                Assert.IsTrue(users.Verify(userDT));
                Assert.IsTrue(materials.Verify(materialDT));
                Assert.IsTrue(components.Verify(componentDT));

                Assert.IsFalse(users.Verify(materialDT));
                Assert.IsFalse(materials.Verify(componentDT));
                Assert.IsFalse(components.Verify(userDT));
                Assert.IsFalse(users.Verify(componentDT));
                Assert.IsFalse(materials.Verify(userDT));
                Assert.IsFalse(components.Verify(materialDT));
            }
        }

        [TestMethod()]
        [DeploymentItem(@"files\Users.csv")]
        public void AccessThroughDataType()
        {
            using (DataSource users = new CSVDataSource("Users.csv"))
            {
                var dataType = new UsersDataType();
                var result = users.ParseRows(dataType).Skip(50).Take(1).First();
                Assert.AreEqual(51, result.id);
                Assert.AreEqual("CodeDa9", result.client_code);
            }
        }
    }
}



